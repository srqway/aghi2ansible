# Exercises

## Import the Ubuntu VM

1. Using the provided USB stick, import the Ubuntu-LTS.OVA
- Start the machine and log in using the provided password
- Verify Internet connectivity

## Git the class material.

1. Open a terminal.
  * [Control]+[Alt]+T
  * Right-click the desktop and choose "Open Terminal"
  * Tap the Windows/Super key, type "term[Enter]"

- Run the following commands.

  ```shell
  wget sofree.us/bits/git-a-class
  # Optionally, read it
  # view git-a-class
  chmod +x git-a-class
  ./git-a-class aghi2ansible
  # Online mats open in web browser
  # Local mats open in file browser
  ```

- Verify that you see the class README in a web-browser

## Install Ansible and use a few ad-hoc commands.

Install Ansible

```bash
sudo apt-add-repository ppa:ansible/ansible
sudo apt update && sudo apt install ansible
```

Ansible ping the local machine

`ansible localhost -m ping`

Use Ansible to browse available facts about the local machine

`ansible localhost -m setup`

## Use an Ansible playbook to install and configure AWS CLI

```bash
cd ~/git/aghi2ansible/ansible_aws/
ansible-playbook awscli-setup.yml -K --ask-vault-pass
```

## Run an Ansible playbook to create 2 machines in AWS

First, log into the [AWS Console](https://sofreeus.signin.aws.amazon.com/console)

account ID: sofreeus
IAM user name: itntl
password: (on white board)

Click 'EC2' link, then click 'running instances'
Your instances are not there! Now create them...

```bash
# unique-ify the playbook template!!
# DO NOT SKIP THIS STEP!

gedit aws-create-instances.yml

# look for the words 'CHANGE ME!!!' make changes and save file


# then run the playbook
ansible-playbook aws-create-instances.yml
```

Behold! Your cromulent instances are now present in the [AWS Console](https://sofreeus.signin.aws.amazon.com/console)

Copy your two IPV4 addresses from the AWS EC2 console and paste them into the hosts file

```bash
gedit hosts
```

## Run Ansible ad-hoc commands on the 2 machines

First unzip ansible_class_secrets.zip files

```bash
unzip ansible_class_secrets.zip # give the password when prompted

# list out the contents of the folder to see that youo now have an unzipped version of the folder called ansible_class_secrets
ls

# go into the new folder
cd ansible_class_secrets

# list the contents, you will see only one file called ansible.pem
ls

# copy that file to the folder above it
cp ansible.pem ../

# go back to the working directory
cd ../

# list the contents again to be sure that the file you copied (ansible.pem) is really there

ls

# now you're ready to do the ad hoc exercises...
```

```bash
# update the host-key of each new server created
# so your ansible controller (ie: your laptop's ~/.ssh/known_hosts file)
# is aware of each new server
ansible webservers -i hosts --list | ./update-host-key.sh
```

```bash
# ping the new servers listed in your hosts file:
ansible -i hosts all --private-key=ansible.pem -u ec2-user -m ping
```

```bash
# check storage capacity of the new servers listed in your hosts file:
ansible -i hosts all --private-key=ansible.pem -a "df -h" -u ec2-user
```

## Modify 2 machines to become Web Servers

Before runing this playbook, copy one of the IP addresses from your hosts file and paste in a browser. No website appears!

Now go back to your bash shell and run the below playbook:

```bash
ansible-playbook make_webservers.yml -i hosts
```
Go back to a browser and paste the IP address again.. your website is now up and running!

Contemplate just how difficult it used to be to get to this point in the olden days and how easy it is now.
