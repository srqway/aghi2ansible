# Check Requisites

## web browser

Open the class materials:

[A Gentle, Hands-on Intro to Ansible](https://www.gitlab.com/sofreeus/aghi2ansible)

```bash
xdg-open https://www.gitlab.com/sofreeus/aghi2ansible
```

## gitlab.com account and 1+ authorized keys

Make sure you have at least one key listed, like Alex and David do:
- https://www.gitlab.com/dlwillson.keys
- https://www.gitlab.com/alexwise.keys

If you don't, go authorize a public key:

[GitLab Account Settings](https://gitlab.com/profile/keys)

You can display your default public key like this:

```bash
cat ~/.ssh/id_rsa.pub
```

If you don't have a key pair, do this:

```bash
ssh-keygen
```

IMPORTANT: Protect *personal* private keys with a passphrase, so that it's harder for others to impersonate you.

## ssh

IMPORTANT:
- Students must give their GitLab usernames to the teacher.
- Students must have access to a private ssh key that matches a public ssh key on the named GitLab account.
- Teacher must add users to `teacher-tools/host_vars/ansible-controller.sofree.us.yml`.
- Teacher must run `teacher-tools/update-linux-users.yml` to add new students.

```bash
ssh-add -L # verify that you have a keyring and a key on it.
ssh -A ansible-controller.sofree.us
```

If you're local username doesn't match your GitLab username, you'll need to add these lines to your ssh config.

Put your GitLab username, not $GITLAB_USERNAME.

```
Host ansible-controller.sofree.us
  User $GITLAB_USERNAME
```

Or, specify your username when connecting.

```bash
ssh -A $GITLAB_USERNAME@ansible-controller.sofree.us
```

## Finally, ssh agent

You'll need access to your private key in your session on the Ansible Controller. It's how you'll prove your majesty to all your machines.

The easy way to do it is to add `-A` to your ssh command when connecting to the Ansible Controller.

```bash
ssh -A ansible-controller.sofree.us
```

For that to work, you have to have ssh-agent running and a key loaded.

Do you have a key loaded?

```bash
ssh-add -L
```

If not, load one.

```bash
ssh-add
```

For that to work, you'll have to have an ssh-agent running.

Do you have an ssh agent running?

```bash
echo $SSH_AUTH_SOCK
```

If not, you'll have to start one.

```bash
eval $( ssh-agent )
# and add a key
ssh-add
# and ssh to the ansible-controller
ssh -A ansible-controller.sofree.us
```

If none of this is working, because your ssh client is crap, you'll need to start ssh-agent and load a key after you log in to the Ansible Controller.

You can do that one of two ways:

1. Copy your current private ssh key to the ansible-controller and use it there.

   CAUTION: Don't do this if your private key is unencrypted! *Never* leave an unencrypted, personal private key on a shared machine.

   ```bash
   scp ~/.ssh/id_rsa ansible-controller.sofree.us:
   ssh ansible-controller.sofree.us
   eval $( ssh-agent )
   ssh-add id_rsa
   ```

2. Create an ssh key pair on the Ansible Controller.

   ```bash
   ssh ansible-controller.sofree.us
   ssh-keygen
   # IMPORTANT: This is a *personal* key pair. Protect the private key with a strong passphrase.
   eval $( ssh-agent )
   ssh-add
   ```

   For this new key pair to work as your ID in later labs, you must now authorize the public key in [your GitLab settings](https://gitlab.com/profile/keys).

   ```bash
   cat ~/.ssh/id_rsa.pub
   ```
