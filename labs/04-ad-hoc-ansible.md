## Exercise: Some Ad-hoc Operations

Check whether I'm a user on a system using "raw" expressions.

```bash
ansible $PRIVATE_IP -a "getent passwd $USER"
ansible $PRIVATE_IP -a "grep $USER /etc/passwd"
ansible $PRIVATE_IP -a "id $USER"
```

Get the private *and* public IP addresses for one of your machines.

Point the following ad-hoc ops at the private IP address.

```bash
ansible $PRIVATE_IP -m package -a "name=httpd" -b
ansible $PRIVATE_IP -m service -a "name=httpd state=started enabled=yes" -b
```

First, run one of those expressions again. Behold how majestic and idempotent
is the package module. Woo.

Now, manually point your web-browser at $PUBLIC_IP and/or run one of the
following.

```bash
curl $PUBLIC_IP
xdg-open $PUBLIC_IP
```

You made a web-server!

Now, tear it all down.

```bash
ansible $PRIVATE_IP -m service -a "name=httpd state=stopped" -b
ansible $PRIVATE_IP -m package -a "name=httpd state=absent" -b
```

Run *that* again.

The package command is idempotent. Still majestic. Woo, yah.
But, the service command failed. Why?
Hint: Does the service even still exist?

Also note: [The website is down](http://thewebsiteisdown.com/).

---

Use the "fetch" module to fetch all the sshd configs.

```bash
ansible all -m fetch -a "src=/etc/ssh/sshd_config dest=/tmp/" -b
```

And again, Anakin. More idempotence. More majesty.

But, what did that do?

```bash
less /tmp/$PRIVATE_IP/etc/ssh/sshd_config
```

---

Now, fetch all the /var/log/messageses, precious.

```bash
ansible all -m fetch -a "src=/var/log/messages dest=/tmp/" -b
```

Notice that this one *never* goes stable. Hmm... I wonder why...

WOULD YOU LIKE TO KNOW
<blink>[MORE](https://docs.ansible.com/ansible/2.9/user_guide/intro_adhoc.html)</blink>?
