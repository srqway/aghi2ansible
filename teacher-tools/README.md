`aws-create-ansible-controller.yml` creates a Fedora 31 machine

Requires:
- awscli and much boto installed
  * ubuntu:
    ```bash
    sudo apt -y install python{,3}-boto{,3,core}
    ```
  * fedora:
    ```bash
    dnf -y install ansible awscli python3-boto3 python3-boto
    ```
- awscli credentials configured and using us-west-2
  ```bash
  aws configure --profile=sfs
  export AWS_PROFILE=sfs
  ```

Do all the setup in the `teacher-tools` folder!

```bash
cd teacher-tools/
```

Source the rc file

```bash
. rc
```

Ensure the ec2-admin user exists in AWS.

```bash
ansible-playbook teacher-tools/aws-create-ec2-admin-bot.yml
```

Ensure the Ansible Controller machine exists in AWS.

```bash
ansible-playbook teacher-tools/aws-create-ansible-controller.yml
```

Manually add the DNS record for the Ansible Controller at Gandi.

`ansible-controller.sofree.us A 123.45.67.90`

Update admins and users in `host_vars/ansible-controller.sofree.us.yml`.

Add Linux admins as `fedora`

```bash
./add-linux-admins.sh fedora ansible-controller.sofree.us ansible-controller.key
```

Update Linux admins and drop `fedora` as yourself.

```bash
ansible-playbook update-linux-admins.yml --extra-vars="target=ansible-controller.sofree.us"
```

Add Ansible, AWS CLI, and whatever else is needed.

```bash
ansible-playbook configure-ansible-controller.yml
```

Add ec2-admin settings and a shallow clone of this repo to /etc/skel/

```
$ ssh ansible-controller.sofree.us
[dlwillson@ip-172-31-40-160 ~]$ aws configure
AWS Access Key ID [None]: AKI<snip>DWV
AWS Secret Access Key [None]: edx<snip>mhZ
Default region name [None]: us-west-2
Default output format [None]: json
[dlwillson@ip-172-31-40-160 ~]$ sudo cp -rv .aws/ /etc/skel/
'.aws/' -> '/etc/skel/.aws'
'.aws/credentials' -> '/etc/skel/.aws/credentials'
'.aws/config' -> '/etc/skel/.aws/config'
[dlwillson@ip-172-31-40-160 ~]$ curl -JLO www.sofree.us/bits/git-a-class
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    73  100    73    0     0    445      0 --:--:-- --:--:-- --:--:--   445
100  1407  100  1407    0     0   2710      0 --:--:-- --:--:-- --:--:--  2710
[dlwillson@ip-172-31-40-160 ~]$ less git-a-class
[dlwillson@ip-172-31-40-160 ~]$ ./git-a-class aghi2ansible
Cloning into 'aghi2ansible'...
<snip>
[dlwillson@ip-172-31-40-160 ~]$ sudo cp -r sfs /etc/skel/
[dlwillson@ip-172-31-40-160 ~]$ exit
logout
Connection to ansible-controller.sofree.us closed.
```

Update Linux users *last*.

```bash
ansible-playbook update-linux-users.yml --extra-vars="target=ansible-controller.sofree.us"
```

Verify that a user can `git pull` and do ec2 ops, but not iam ops.

```bash
sudo su - someuser
cd sfs/aghi2ansible/
git pull
aws iam list-users # should error
aws ec2 get-console-screenshot --instance-id i-0db6c1820c57b061e
```

TODO:

```bash
sudo hostnamectl set-hostname Ansible-Controller.SoFree.Us
sudo dnf -y install vim python3-boto3
sudo dnf -y distro-sync && sudo reboot
```

Notes:

```bash
aws ec2 start-instances --instance-id i-0db6c1820c57b061e
```
