Before class: Make ansible-controller

## Exercise: Introductions and team formation

## Exercise: Setup identifiers for class

me: Use the blank Fedora box.

Clone this repository

```bash
cd ~/git/ # or cd where-you-keep-your-code
git clone git@gitlab.com:sofreeus/aghi2ansible.git
# or
git clone https://gitlab.com/sofreeus/aghi2ansible.git
```

Update host keys for the machines

```bash
ansible all -i hosts.raw --list |
./update-host-key.sh
```

Decrypt vaulted pem-file

```bash
cp ansible.pem.vault ansible.pem
ansible-vault decrypt ansible.pem
# password: "SoF*****-**17!"
```

Verify ssh connectivity

```bash
ssh -i ansible.pem centos@52.41.28.89
```

Review inventory, assign machines, rename your group to 'raw'

Ansible ping the machines

```bash
export ANSIBLE_INVENTORY=hosts.raw
ansible all -m ping
ansible raw -m ping
```

## Exercise: Add admins

1. Edit hosts.raw: Delete all machines except your team's.
*  Edit add-admins.yml to correct name/keyu values.
*  Run  ./add-admins.sh

## Discuss: trifecta (install, configure, start)

## Exercise: Make a web-server

1. Copy hosts.raw to hosts.myteam (see hosts.alicorn)
*  Edit hosts.myteam:
  - Remove everything after the IP address.
  - Add groups.
*  Edit ansiblerc and source it `. ansiblerc`
*  or `export ANSIBLE_INVENTORY=hosts.myteam`
*  Edit `webserver-playbook.yml`
*  Run `ansible-playbook webserver-playbook.yml`

## Discuss: round-robin DNS, HA/HP, SSL termination

## Exercise: Make an SSL-terminating proxy / load-balancer

* need internal IP's of app-servers
* need to loop through the app-servers in the template

## Discuss: auto-scaling

## Exercise: Make more web-servers. Add them to the load-balancer automatically. And, chaos-monkey them.

* need to gather internal ip addresses *automatically*.
